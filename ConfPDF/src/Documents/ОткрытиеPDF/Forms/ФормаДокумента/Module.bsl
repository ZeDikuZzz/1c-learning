Перем ИмяВременногоФайла;

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	//HTML = 
	//"<html>
	//|	<body style=""margin:0;padding:0"">
	//|		<iframe src=" + Объект.Макеты.Макет + " width=""100%"" height=""100%""></iframe>
	//|	</body>
	//|</html>"
	О = РеквизитФормыВЗначение("Объект");
	 //HTML = СтрЗаменить(
	 // 	О.ПолучитьМакет("Макет").ПолучитьТекст(),
	 //   "<base64pdf>",
	 //   ПолучитьBase64СтрокуИзДвоичныхДанных(О.ПолучитьМакет("PDF"))
	 // );
	
	//ВременныйPDF = ПолучитьPDFФайл();
	                                                                                                                         
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла(СтрЗаменить(Новый УникальныйИдентификатор, "-", "") + ".pdf");
	Данные = О.ПолучитьМакет("PDF");
	Данные.Записать(ИмяВременногоФайла);

	
  	HTML = СтрЗаменить(О.ПолучитьМакет("Макет").ПолучитьТекст(),"<TextScript111>",О.ПолучитьМакет("СкриптБиблиотека").ПолучитьТекст());
  
  	HTML = СтрЗаменить(HTML,"<TextScript112>",О.ПолучитьМакет("СкриптWorker").ПолучитьТекст());
	  
	HTML = СтрЗаменить(HTML,"<url1S>","file:///"+СтрЗаменить(ИмяВременногоФайла, "\", "//"));


	//Файл = Новый ЗаписьТекста("D:\new 39.html");
	//Файл.ЗаписатьСтроку(HTML);
	//Файл.Закрыть();

КонецПроцедуры

&НаСервере
Функция СоздатьPDFФайлНаСеврере()
	_word = New COMОбъект("Word.Application");
	_word.DisplayAlerts = 0; 
	doc = _word.Documents.Open("D:\Git.docx"); 
	wdExportFormatPDF = 17;
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла(СтрЗаменить(Новый УникальныйИдентификатор, "-", "") + ".pdf");
	doc.ExportAsFixedFormat (ИмяВременногоФайла, wdExportFormatPDF, 0, false, 0, 1, 1, 0, True, True, 0, True, True, False);
	doc.Close(False);
	_word.Quit();
	ДвоичныеДанныеФайла = Новый ДвоичныеДанные(ИмяВременногоФайла);
	Возврат ДвоичныеДанныеФайла
КонецФункции

&НаСервере
Процедура ПередЗакрытиемНаСервере()
	Попытка
		//УдалитьФайлы("D:\new 39.html");
		УдалитьФайлы(ИмяВременногоФайла);
	Исключение;
	КонецПопытки;
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	ПередЗакрытиемНаСервере();
КонецПроцедуры


