
&НаСервере
Процедура СозданиеНаСервере()
	_word = New COMОбъект("Word.Application");
	_word.DisplayAlerts = 0; 
	doc = _word.Documents.Open("D:\Git.docx"); 
	wdExportFormatPDF = 17;
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла(СтрЗаменить(Новый УникальныйИдентификатор, "-", "") + ".pdf");
	doc.ExportAsFixedFormat (ИмяВременногоФайла, wdExportFormatPDF, 0, false, 0, 1, 1, 0, True, True, 0, True, True, False);
	doc.Close(False);
	_word.Quit();
КонецПроцедуры

&НаКлиенте
Процедура Создание(Команда)
	СозданиеНаСервере();
КонецПроцедуры
